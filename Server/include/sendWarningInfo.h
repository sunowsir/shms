/*
 * File Name:    sendWarningInfo.h
 * Author:       sunowsir
 * Mail:         sunowsir@protonmail.com
 * GitHub:       github.com/sunowsir
 * Created Time: 2018年12月16日 星期日 12时00分28秒
 */

#ifndef _SENDWARNINGINFO_H
#define _SENDWARNINGINFO_H

#include "../../Share/include/needHead.h"
#include "../../Share/include/sockFrame.h"
#include "../../Share/include/Get_conf.h"
#include "../../Share/include/StrtoInt.h"

/* return : success(0) or false(-1)  */

int sendWarningInfo(char *);

#endif
