/*
 * File Name:    heartBeat.h
 * Author:       sunowsir
 * Mail:         sunowsir@protonmail.com
 * GitHub:       github.com/sunowsir
 * Created Time: 2018年11月23日 星期五 16时25分29秒
 */

#ifndef _HEARTBEAT_H
#define _HEARTBEAT_H

#include "./dataTransmission.h"
#include "../../Share/include/StrtoInt.h"
#include "../../Share/include/Get_conf.h"
#include "../../Share/include/needHead.h"
#include "../../Share/include/sockFrame.h"

void *heartBeat();

#endif


